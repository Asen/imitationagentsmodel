
class Point(object):

	daily_attacks_try = 0;
	daily_attacks_succ = 0;
	daily_attacks_prev = 0;
	daily_reqs = 0;
	day_number = 0;
	cost = 0

	def __init__(self, dayNum, succAttacks, tryAttacks, prevAttacks, reqs, cost):
		self.daily_attacks_try = tryAttacks;
		self.daily_attacks_succ = succAttacks;
		self.daily_attacks_prev = prevAttacks;
		self.daily_reqs = reqs;
		self.day_number = dayNum;
		self.cost = cost;