from enum import Enum;
import random


class Vuln(object):

	skill_to_exploit = 0  # необходимое умение для эксплуатации
	lifeTime = 0;         # время жизни уязвимости

	kind = None           # тип нарушения безопасности
	exploiting = None     # тип эксплуатации

	def __init__(self, vulnType):
		self.kind = vulnType;


	def toString(self):
		print("Type = "+str(self.kind));


	def initWithParams(self, lifeTime, skill_to_exploit):
		self.lifeTime = lifeTime;
		self.skill_to_exploit = skill_to_exploit;
		return self;


	def createCodeInjection(lifetime, skill):
		vuln = Vuln(VulnType.CodeInjection).initWithParams(lifetime, skill);
		vuln.exploiting = ExploitType.getExploitType(40,40,20);
		return vuln;
	def createPhysicalAccess(lifetime, skill):
		vuln = Vuln(VulnType.PhysicalAccess).initWithParams(lifetime, skill);
		vuln.exploiting = ExploitType.getExploitType(10,45,45);
		return vuln;
	def createBinary(lifetime, skill):
		vuln = Vuln(VulnType.Binary).initWithParams(lifetime, skill);
		vuln.exploiting = ExploitType.getExploitType(40,40,20);
		return vuln;
	def createLogic(lifetime, skill):
		vuln = Vuln(VulnType.Logic).initWithParams(lifetime, skill);
		vuln.exploiting = ExploitType.getExploitType(40,50,10);
		return vuln;


class VulnType(Enum):
	CodeInjection = 1;     # sqli, rce, xss
	PhysicalAccess = 2     # физический доступ 
	Binary = 3             # низкоуровневая бинарная уязвимость
	Logic = 4              # логическая ошибка


class ExploitType(Enum):
	Confidentiality = 1;
	Integrity = 2;
	Availability = 3;

	def getExploitType(confidentiality, integrity, availability):
		exploit_type = random.randint(0,100);
		if 0<=exploit_type<confidentiality:
			return ExploitType.Confidentiality; 
		elif confidentiality<=exploit_type<confidentiality+integrity:
			return ExploitType.Integrity;
		elif confidentiality+integrity<=exploit_type<100:
			return ExploitType.Availability; 