from component import Component
from vuln import Vuln
from vuln import VulnType
from vuln import ExploitType
from point import Point
import random

class ServerInfr(object):

	WEB_SERVER = "WebServer"
	FS         = "FS"
	DB         = "DB"
	OS         = "OS"
	PHYS_LVL   = "PHYS_LVL"


	components = {}

	succAttacks = 0;
	tryAttacks = 0;
	prevented = 0;
	daily_attacks_try = 0;
	daily_attacks_succ = 0;
	daily_attacks_prev = 0;
	daily_reqs = 0;
	day_number = 0;
	attTypes={}
	expTypes={}
	statistics = []

	secured = False;


	def __init__(self, is_secured):

		self.succAttacks = 0;
		self.tryAttacks = 0;
		self.prevented = 0;

		self.daily_attacks_try = 0;
		self.daily_attacks_succ = 0;
		self.daily_attacks_prev = 0;
		self.daily_reqs = 0;
		self.day_number = 0;

		self.attTypes={VulnType.CodeInjection:0, VulnType.Binary:0, VulnType.PhysicalAccess:0, VulnType.Logic:0};
		self.expTypes={ExploitType.Confidentiality:0, ExploitType.Integrity:0, ExploitType.Availability:0};

		self.statistics = [];

		self.secured = is_secured;


	def loadComponents(self):
		self.components[self.WEB_SERVER] = Component(self.WEB_SERVER, 50)
		self.components[self.DB] = Component(self.DB, 0);
		self.components[self.FS] = Component(self.FS, 0)
		self.components[self.OS] = Component(self.OS, 100)
		self.components[self.PHYS_LVL] = Component(self.PHYS_LVL, 5)


	def sendAction(self, component_name, action):

		self.daily_reqs += 1;

		self.components.get(component_name).sendAction(action);

		self.components.get(self.WEB_SERVER).relative_cost = \
			self.calcRelativeCost([self.FS, self.DB]);

		self.components.get(self.OS).relative_cost = \
			self.calcRelativeCost([self.FS, self.DB, self.WEB_SERVER]);

		self.components.get(self.PHYS_LVL).relative_cost = \
			self.calcRelativeCost([self.FS, self.DB, self.WEB_SERVER, self.OS]);

		if action.is_malicious:
			skill = int(random.expovariate(1.0))      # down exp 0..10 levels
			if skill<0: skill=0;
			if skill>9: skill=10;
			self.badGuyTryExploit(action, skill);


	def badGuyTryExploit(self, action, skill):

		exComponent = None;
		exComponentVuln = None;
		exProfit = 0;

		for componentName in self.components.keys():
			for vuln in self.components[componentName].vulns:
				if vuln.skill_to_exploit<=skill:
					if self.components[componentName].getRelativeCost() > exProfit:
						exComponent = componentName;
						exComponentVuln = vuln;
						exProfit = self.components[componentName].getRelativeCost()

		# попытка атаки
		self.tryAttacks += 1;
		self.daily_attacks_try += 1;

		# эксплоит найден и будет использован
		if exComponent!=None:

			# можно ли предотвратить действия злоумышленника?
			if self.secured and not self.canExploit(exComponentVuln):
				self.prevented += 1;
				self.daily_attacks_prev += 1;
				print("############ Attack is prevented = "+str(exComponent)+" : "+str(exComponentVuln.kind));
				return;
			
			if exComponentVuln.exploiting==ExploitType.Confidentiality:
				print("Minus"+str(int(self.components[exComponent].total_cost * 0.1)));
				self.components[exComponent].total_cost -= int(self.components[exComponent].total_cost * 0.1)
			if exComponentVuln.exploiting==ExploitType.Integrity:
				print("Zeroed");
				self.components[exComponent].total_cost = 0;
			if exComponentVuln.exploiting==ExploitType.Availability:
				print("Minus"+str(int(self.components[exComponent].total_cost * 0.5)));
				self.components[exComponent].total_cost -= int(self.components[exComponent].total_cost * 0.5)
			
			self.attTypes[exComponentVuln.kind] += 1;
			if not exComponentVuln.exploiting==None:
				self.expTypes[exComponentVuln.exploiting] += 1

			self.succAttacks += 1;
			self.daily_attacks_succ += 1;
			self.components[exComponent].vulns.remove(exComponentVuln);	
			print("###### Attacker with skill = "+str(skill)+" Exploited: "+str(exComponent)+" with "+str(exComponentVuln.kind)+"="+str(exComponentVuln.exploiting));


	def canExploit(self, vuln):
		
		can = 1.0;
		
		if   vuln.kind == VulnType.Binary:
			# агент-файрвол
			if self.isFirewallOk():
				can *= 0.8
			# агент-антивирус
			if self.isAntivirusOk():
				can *= 0.5
			# низкоуровневый агент
			if self.isLowLevelSoftwareSecyrutyOk():
				can *= 0.5;
			if self.isSandBoxOk():
				can *= 0.1;

		elif vuln.kind == VulnType.CodeInjection:
			# агент-файрвол
			if self.isFirewallOk():
				can *= 0.7
			# агент-антивирус
			if self.isAntivirusOk():
				can *= 0.5
			# агент-печоница прав
			if self.isSandBoxOk():
				can *= 0.1;

		elif vuln.kind == VulnType.Logic:
			# агент-файрвол
			if self.isFirewallOk():
				can *= 0.9
			# агент-печоница прав
			if self.isSandBoxOk():
				can *= 0.95;

		elif vuln.kind == VulnType.PhysicalAccess:
			# агент-охранник
			if self.isSecurityGuyOk():
				can *= 0.5;
			# агент-замОк
			if self.isLocksOk():
				can *= 0.7;
			# агент-хардварная защита
			if self.isHardwareSecurityOk():
				can *= 0.2;

		print("Can EXPLOIT = "+str(100*can))
		return 0 <= random.randint(0,100) <= 100.0 * can;



	################################
	#
	#         Агенты защиты
	#
	################################
	def isFirewallOk(self):
		# вероятность того, что файрволл работает нормально = 97%
		return 1<=random.randint(0,100)<=97;
	def isAntivirusOk(self):
		# вероятность того, что антивирусная защита работает = 93%
		return 1<=random.randint(0,100)<=93;
	def isSandBoxOk(self):
		# вероятность того, что печоница, в которой работает ПО работает как надо = 93%
		return 1<=random.randint(0,100)<=30;
	def isLowLevelSoftwareSecyrutyOk(self):
		# вероятность того, что в ПО при компиляции были включены всякие ASLR`ы = 50%
		return 1<=random.randint(0,100)<=50;
	def isSecurityGuyOk(self):
		# вероятность того, что охранник на стороже = 50%
		return 1<=random.randint(0,100)<=90;
	def isLocksOk(self):
		# вероятность того, что замки на дверях висят = 75%
		return 1<=random.randint(0,100)<=85;
	def isHardwareSecurityOk(self):
		# вероятность того, что вся серверная аппаратура защищена серверной стойкой = 80%
		return 1<=random.randint(0,100)<=80;


	def newDay(self):

		self.dayFinished();
		self.daily_attacks_try = 0;
		self.daily_attacks_succ = 0;
		self.daily_attacks_prev = 0;
		self.daily_reqs = 0;
		self.day_number += 1;

		for componentName in self.components.keys():

			CodeInjection = PhysicAccess = Binary = Logic = 1;

			if  componentName == self.WEB_SERVER:
				CodeInjection  = 10 
				PhysicAccess   = 14 
				Binary         = 10  
				Logic          = 60 
			elif componentName == self.FS:
				CodeInjection  = 30  
				PhysicAccess   = 7   
				Binary         = 12  
				Logic          = 45  
			elif componentName == self.OS:
				CodeInjection  = 50  
				PhysicAccess   = 7   
				Binary         = 25  
				Logic          = 50
			elif componentName == self.DB:
				CodeInjection  = 7  
				PhysicAccess   = 7   
				Binary         = 30  
				Logic          = 45 
			elif componentName == self.PHYS_LVL:
				CodeInjection  = 0  
				PhysicAccess   = 7   
				Binary         = 0  
				Logic          = 0 


			# codeinjection
			if CodeInjection != 0 and 0 <= random.randint(0,100) <= 100.0/CodeInjection:
				lifetime = int(random.expovariate(0.25)) + 3; # down exp 3..13 days
				skill = 10-int(random.expovariate(0.55))      # up exp 0..10 levels
				if skill<1: skill=1;
				self.components[componentName].addVuln(Vuln.createCodeInjection(lifetime, skill));
			# physicalaccess
			if PhysicAccess != 0 and 0 <= random.randint(0,100) <= 100.0/PhysicAccess:
				lifetime = int(random.expovariate(1.25)) + 1; # exp from 1..4 days
				skill = 10-int(random.expovariate(0.45))      # up exp 0..10 levels
				if skill<1: skill=1;
				self.components[componentName].addVuln(Vuln.createPhysicalAccess(lifetime, skill));
			# binary
			if Binary != 0 and 0 <= random.randint(0,100) <= 100.0/Binary:
				lifetime = int(random.expovariate(0.10)) + 1; # exp from 1..31 days
				skill = 10-int(random.expovariate(0.90))      # up exp 0..10 levels
				if skill<1: skill=1;
				self.components[componentName].addVuln(Vuln.createBinary(lifetime, skill));
			# logic
			if Logic != 0 and 0 <= random.randint(0,100) <= 100.0/Logic:
				lifetime = int(random.expovariate(0.10)) + 1;  # exp from 1..31 days
				skill = 10-int(random.expovariate(0.65))       # up exp 0..10 levels
				if skill<1: skill=1;
				self.components[componentName].addVuln(Vuln.createLogic(lifetime, skill));

		for componentName in self.components.keys():
			for vuln in self.components[componentName].vulns:
				vuln.lifeTime -= 1;
				if vuln.lifeTime<=0:
					self.components[componentName].vulns.remove(vuln);


	def dayFinished(self):

		self.statistics.append(Point(\
			dayNum=self.day_number,\
			succAttacks=self.daily_attacks_succ,\
			tryAttacks=self.daily_attacks_try,\
			prevAttacks=self.daily_attacks_prev,\
			reqs=self.daily_reqs,\
			cost=self.calcTotalCost()\
			))

		if self.day_number%50 == 0:
			dayNumFile = open("stat/num.txt", "a");
			attTryFile = open("stat/atttry.txt", "a");
			attPrevFile = open("stat/attprev.txt", "a");
			attSuccFile = open("stat/attsucc.txt", "a");
			reqFile = open("stat/reqs.txt", "a");
			costFile = open("stat/cost.txt", "a");

			for point in self.statistics:
				dayNumFile.write(str(point.day_number)+"\n");
				attTryFile.write(str(point.daily_attacks_try)+"\n");
				attPrevFile.write(str(point.daily_attacks_prev)+"\n");
				attSuccFile.write(str(point.daily_attacks_succ)+"\n");
				reqFile.write(str(point.daily_reqs)+"\n");
				costFile.write(str(point.cost)+"\n");

			dayNumFile.close();
			attTryFile.close();
			attPrevFile.close();
			attSuccFile.close();
			reqFile.close();
			costFile.close();

			self.statistics.clear();



	def calcRelativeCost(self, components):

		relative_cost = 0;

		for name in components:
			relative_cost += self.components[name].getOwnCost();

		return relative_cost;


	def calcTotalCost(self):
		total_cost = 0;
		for name in self.components.keys():
			total_cost += self.components[name].getOwnCost();

		return total_cost;


	def printComps(self):

		for key in self.components.keys():
			c = self.components[key]
			print(key+" => Ocost:"+str(c.getOwnCost())+"; Rcost:"+str(c.getRelativeCost())+"; reqs:"+str(c.total_reqs)+"; rej:"+str(c.rejected_reqs)+"; vulns:"+str(c.printVulns()))

		print("--------------------------------------");

		for key in self.attTypes.keys():
			print(str(key)+" = "+str(self.attTypes[key]))

		print("--------------------------------------");

		for key in self.expTypes.keys():
			print(str(key)+" = "+str(self.expTypes[key]))