import threading
import random
from vuln import Vuln
from action import Action
from component import Component
from server_infr import ServerInfr


MODEL_DAY_INTERVAL   = 0;
REQS_PER_DAY         = 50000;
REQS_EVIL_PERCENT    = 0.5 
REQS_DEVIATION       = REQS_PER_DAY * 0.25;

infrastruct = ServerInfr(is_secured=False);
daysGone = 0


def genRequests():

	# количество запросов в такт времени (распределено нормально)
	reqsCount = int(random.normalvariate(REQS_PER_DAY, REQS_DEVIATION));
	print(reqsCount)

	for i in range(0, reqsCount):

		is_action_malicious = 0 <= random.randint(0, 10000) <= getMaliciousInfluenceProb()*100;
		action = None;

		if not is_action_malicious:
			act_type = random.randint(0, 100);   # тип воздействия

			if 0<=act_type and act_type<=20:     # POST = 20% от безопасных
				req_cost = +int(random.paretovariate(2.0));
				action = Action(req_cost)
			elif 21<=act_type and act_type<=40:  # DELETE = 20% от безопасных
				req_cost = -int(random.paretovariate(2.0));
				action = Action(req_cost)
			else:                                # GET = 60% от безопасных
				req_cost = 0;
				action = Action(req_cost)
		else:
			action = Action(0);
			action.is_malicious = True;

		req_for = random.randint(0, 100);    # компонент-получатель запроса

		if 00<=req_for and req_for<=65:                     # 60%
			infrastruct.sendAction(ServerInfr.DB, action)
		elif 66<=req_for and req_for<=96:                   # 20%
			infrastruct.sendAction(ServerInfr.FS, action)
		elif 97<=req_for and req_for<=98:
			infrastruct.sendAction(ServerInfr.OS, action)
		elif 99<=req_for and req_for<=100:
			infrastruct.sendAction(ServerInfr.WEB_SERVER, action)
		else:
			infrastruct.sendAction(ServerInfr.PHYS_LVL, action)


def getMaliciousInfluenceProb():

	malicious_probability = REQS_EVIL_PERCENT*(infrastruct.calcTotalCost()/100000.0);
	return min(REQS_EVIL_PERCENT, malicious_probability)


# 1 день в мире модели
def modelFunction():

	global daysGone;
	daysGone += 1
	print("======= Day: "+ \
		str(daysGone)+" ===== Cost: "+str(infrastruct.calcTotalCost())+ \
		" ===== Malic: "+str(getMaliciousInfluenceProb())+\
			"===== Attacks: "+str(infrastruct.succAttacks)+"/"+str(infrastruct.tryAttacks)+"/"+\
			str(infrastruct.prevented)
		);

	genRequests();
	infrastruct.newDay();
	infrastruct.printComps();
	#for component in components:
	#	print(component.toString())
	#print("---------");


def runModel():
	modelFunction()
	threading.Timer(MODEL_DAY_INTERVAL, runModel).start();


def initModel():
	infrastruct.loadComponents();
	

if __name__ == "__main__":
	initModel();
	runModel();