from action import Action


class Component(object):

	name = None;
	vulns = [];

	total_cost = 0;       # собственная информационная стоимость
	relative_cost = 0;    # стоимость низлежащих компонентов    

	total_reqs = 0;
	rejected_reqs = 0;


	def __init__(self, name, initial_cost):
		self.name = name;
		self.total_cost = initial_cost;
		self.vulns = [];


	def toString(self):
		return self.name;


	def printVulns(self):
		vulnsInfo = "\n";
		for vuln in self.vulns:
			vulnsInfo += str(vuln.kind)+" : life="+str(vuln.lifeTime)+"; skill="+str(vuln.skill_to_exploit)+"\n";
		return vulnsInfo;


	def sendAction(self, action):
		
		if self.total_cost+action.cost >= 0:
			self.total_reqs += 1;
			self.total_cost += action.cost
		else:
			self.rejected_reqs += 1;


	def getOwnCost(self):
		return self.total_cost;


	def getRelativeCost(self):
		return self.relative_cost + self.total_cost;


	def addVuln(self, vuln):
		self.vulns.append(vuln)